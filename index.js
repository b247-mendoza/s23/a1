
	let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard","Squirtle", "Bulbasaur"],
		friends: {
			hoen: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		talk: function(){
			console.log(trainer.pokemon[0] + '! I choose you!');
		}
	};
	 console.log(trainer);
	 console.log('Result of dot notation: ');
	 console.log(trainer.name);
	 console.log('Result of square bracket notation:');
	 console.log(trainer['pokemon']);
	 console.log('Result of talk method');
	 trainer.talk();

	
	function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + ' tackled ' + target.name);
			console.log(target.name + " health is now reduced to " + Number(target.health - this.attack));
		};
		this.faint = function(){
			console.log(this.name + ' fainted. ');
		}
	};

		let pikachu = new Pokemon('Pikachu', 12);
		console.log(pikachu);
		let geodude = new Pokemon('Geodude', 8);
		console.log(geodude);
		let mewTwo = new Pokemon('Mewtwo', 100);
		console.log(mewTwo);
		mewTwo.tackle(geodude);
		geodude.faint();
